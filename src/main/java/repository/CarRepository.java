package repository;

import entity.Car;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class CarRepository {

    private Connection connection;


    private static CarRepository instance;

    public static CarRepository getInstance() {
        if (instance == null) {
            instance = new CarRepository();
        }
        return instance;
    }

    private CarRepository(){
        connection = getConnection();
    }


    public List<Car> getCars() {

        List<Car> cars = new ArrayList<>();

        PreparedStatement preparedStatement;

        ResultSet resultSet;

        try {

            preparedStatement = connection.prepareStatement("select * from cars");

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                int id = resultSet.getInt("id");
                String brand = resultSet.getString("brand");
                String model = resultSet.getString("model");
                String engineCode = resultSet.getString("engine_code");
                int horsepower = resultSet.getInt("horsepower");
                int topSpeed = resultSet.getInt("top_speed");
                String colour = resultSet.getString("colour");
                int doors = resultSet.getInt("doors");
                String engineType = resultSet.getString("engine_type");

                Car car = new Car(engineCode, horsepower, topSpeed, id, brand, model, doors, engineType, colour);

                cars.add(car);
            }

            return cars;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }


    public void save(Car car) {

        PreparedStatement preparedStatement;

        try {
            preparedStatement = connection.prepareStatement("insert into cars (brand, model,engine_code, " +
                    "horsepower, top_speed, colour, doors, engine_type) " +
                    "values (?, ?, ?, ?, ?, ?, ?, ?)");


            preparedStatement.setString(1, car.getBrand());
            preparedStatement.setString(2, car.getModel());
            preparedStatement.setString(3, car.getEngineCode());
            preparedStatement.setInt(4, car.getHorsepower());
            preparedStatement.setInt(5, car.getTopSpeed());
            preparedStatement.setString(6, car.getColour());
            preparedStatement.setInt(7, car.getDoors());
            preparedStatement.setString(8, car.getEngineType());

            preparedStatement.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(Car car) {

        PreparedStatement preparedStatement;

        try {
            preparedStatement = connection.prepareStatement("delete from cars where id=?");

            preparedStatement.setInt(1, (int) car.getId());


            preparedStatement.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Car find(Car car) {

        PreparedStatement preparedStatement;
        ResultSet resultSet;

        try {
            String sql = "select * from cars where id=?";

            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setInt(1, (int) car.getId());

            resultSet = preparedStatement.executeQuery();

            Car foundCar;

            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                String brand = resultSet.getString("brand");
                String model = resultSet.getString("model");
                String engineCode = resultSet.getString("engine_code");
                int horsepower = resultSet.getInt("horsepower");
                int topSpeed = resultSet.getInt("top_speed");
                String colour = resultSet.getString("colour");
                int doors = resultSet.getInt("doors");
                String engineType = resultSet.getString("engine_type");


                foundCar = new Car(engineCode, horsepower, topSpeed, id, brand, model, doors, engineType, colour);
                return foundCar;

            } else {
                throw new Exception("Could not find Car with id: " + car.getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void modify(Car car) {

        PreparedStatement preparedStatement;
        try {

            preparedStatement = connection.prepareStatement("update cars set brand=?, model=?,engine_code=?, horsepower=?, top_speed=?, colour=?, doors=?, engine_type=? where id=?");

            preparedStatement.setString(1, car.getBrand());
            preparedStatement.setString(2, car.getModel());
            preparedStatement.setString(3, car.getEngineCode());
            preparedStatement.setInt(4, car.getHorsepower());
            preparedStatement.setInt(5, car.getTopSpeed());
            preparedStatement.setString(6, car.getColour());
            preparedStatement.setInt(7, car.getDoors());
            preparedStatement.setString(8, car.getEngineType());
            preparedStatement.setInt(9, (int)car.getId());

            preparedStatement.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Connection getConnection() {
        try {
            String databaseUrl = "jdbc:mysql://localhost:3306/cars";
            String user = "skillversum";
            String password = "password";

            return DriverManager.getConnection(databaseUrl, user, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
