package managedbeans;


import entity.Car;
import repository.CarRepository;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ApplicationScoped
public class CarManagedBean {


    private CarRepository carRepository;

    private List<Car> carList = new ArrayList<>();

    private Car selectedCar = new Car();

    private List<String> colorList  = new ArrayList<>();
    private List<Integer> doorOptions  = new ArrayList<>();


    public CarManagedBean() {
    carRepository = CarRepository.getInstance();
    }


    @PostConstruct
    public void init(){
        colorList.add("Red");
        colorList.add("Blue");
        colorList.add("Green");
        colorList.add("Yellow");
        doorOptions.add(2);
        doorOptions.add(3);
        doorOptions.add(4);
        doorOptions.add(5);
        carList = carRepository.getCars();
    }


    public void save(Car car){
        if(car.getId()<=0) {
            carRepository.save(car);
            selectedCar = new Car();
            loadCarList();
        }
        else{
            carRepository.modify(car);
            selectedCar = new Car();
            loadCarList();
        }
    }

    private void loadCarList(){
        carList.clear();
        carList = carRepository.getCars();
    }

    public void delete(Car car) {
       carRepository.delete(car);
       loadCarList();
    }

    public void findCar(Car car){
        selectedCar = carRepository.find(car);
    }


    public List<Car> getCarList() {
        return carList;
    }

    public void setCarList(List<Car> carList) {
        this.carList = carList;
    }

    public Car getSelectedCar() {
        return selectedCar;
    }

    public void setSelectedCar(Car selectedCar) {
        this.selectedCar = selectedCar;
    }


    public List<String> getColorList() {
        return colorList;
    }

    public void setColorList(List<String> colorList) {
        this.colorList = colorList;
    }

    public List<Integer> getDoorOptions() {
        return doorOptions;
    }

    public void setDoorOptions(List<Integer> doorOptions) {
        this.doorOptions = doorOptions;
    }

}
