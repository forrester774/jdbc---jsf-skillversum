package com.skillversum.JavaServerFacesDemoApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaServerFacesDemoAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaServerFacesDemoAppApplication.class, args);
	}
}
